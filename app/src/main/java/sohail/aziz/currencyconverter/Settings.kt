package sohail.aziz.currencyconverter

object Settings {

    const val API_BASE_URL = "https://revolut.duckdns.org"
    const val REFRESH_INTERVAL_IN_SECONDS = 1L
    const val DEFAULT_CURRENCY="EUR"
}