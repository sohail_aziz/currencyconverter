package sohail.aziz.currencyconverter.presentation.ui.presenter

import com.trello.rxlifecycle2.android.ActivityEvent
import sohail.aziz.currencyconverter.Settings.DEFAULT_CURRENCY
import sohail.aziz.currencyconverter.domain.interactor.DefaultSubscriber
import sohail.aziz.currencyconverter.domain.interactor.GetRatesUseCase
import sohail.aziz.currencyconverter.presentation.internal.PerActivity
import sohail.aziz.currencyconverter.presentation.models.RateModel
import sohail.aziz.currencyconverter.presentation.ui.view.IMainView
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

@PerActivity
class MainPresenter
@Inject constructor(
    private val mainView: IMainView,
    private val getRatesUseCase: GetRatesUseCase
) : IMainPresenter {

    init {
        loadRates()
    }


    private fun loadRates() {
        Timber.d("loadRates")

        mainView.showLoading()
        getRatesUseCase
            .setBaseCurrency(DEFAULT_CURRENCY)
            .unsubscribeOn(ActivityEvent.DESTROY)
            .execute(object : DefaultSubscriber<List<RateModel>>() {
                override fun onError(e: Throwable?) {
                    mainView.hideLoading()
                    mainView.onRatesLoadError(e!!)
                }

                override fun onNext(t: List<RateModel>) {
                    mainView.hideLoading()
                    if (t.isEmpty()) { //assumption, empty list means network error
                        mainView.onRatesLoadError(IOException())

                    } else {
                        mainView.onRatesLoadSuccess(t)
                    }
                }
            })
    }


}