package sohail.aziz.currencyconverter.presentation.ui.view.adapter

import android.view.View
import sohail.aziz.currencyconverter.presentation.models.RateViewModel
import sohail.aziz.currencyconverter.presentation.ui.view.base.AbstractViewHolder

interface IRateViewTypeFactory {

    fun type(rateViewModel: RateViewModel): Int
    fun id(rateViewModel: RateViewModel): Long

    fun createViewHolder(
        itemView: View, type: Int
    ): AbstractViewHolder<out IRateVisitable>
}