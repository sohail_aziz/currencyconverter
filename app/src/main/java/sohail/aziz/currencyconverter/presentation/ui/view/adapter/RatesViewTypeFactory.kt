package sohail.aziz.currencyconverter.presentation.ui.view.adapter

import android.view.View
import sohail.aziz.currencyconverter.presentation.models.RateViewModel
import sohail.aziz.currencyconverter.presentation.ui.view.base.AbstractViewHolder
import sohail.aziz.currencyconverter.presentation.ui.view.base.CurrencyImageFactory
import javax.inject.Inject

class RatesViewTypeFactory
@Inject constructor(
    private val currencyImageFactory: CurrencyImageFactory) : IRateViewTypeFactory {

    override fun type(rateViewModel: RateViewModel): Int = RateViewHolder.LAYOUT

    override fun id(rateViewModel: RateViewModel): Long = rateViewModel.rateModel.hashCode().toLong()


    override fun createViewHolder(itemView: View, type: Int): AbstractViewHolder<out IRateVisitable> {

        return when (type) {
            RateViewHolder.LAYOUT -> RateViewHolder(itemView, currencyImageFactory)
            else -> throw IllegalStateException("invalid type")
        }

    }
}