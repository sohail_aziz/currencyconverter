package sohail.aziz.currencyconverter.presentation.ui.view.adapter

interface IRateVisitable {

    fun type(factory: IRateViewTypeFactory): Int
    fun id(factory: IRateViewTypeFactory): Long
}