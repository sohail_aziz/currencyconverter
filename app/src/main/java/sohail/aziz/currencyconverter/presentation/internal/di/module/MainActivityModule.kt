package sohail.aziz.currencyconverter.presentation.internal.di.module

import dagger.Module
import dagger.Provides
import sohail.aziz.currencyconverter.presentation.internal.PerActivity
import sohail.aziz.currencyconverter.presentation.ui.presenter.IMainPresenter
import sohail.aziz.currencyconverter.presentation.ui.presenter.MainPresenter
import sohail.aziz.currencyconverter.presentation.ui.view.IMainView
import sohail.aziz.currencyconverter.presentation.ui.view.MainActivity


@PerActivity
@Module
class MainActivityModule(private val activity: MainActivity) :
    BaseActivityModule(activity = activity) {

    @PerActivity
    @Provides
    fun provideMainView(): IMainView = activity

    @PerActivity
    @Provides
    fun provideMainPresenter(presenter: MainPresenter): IMainPresenter = presenter

}