package sohail.aziz.currencyconverter.presentation.ui.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sohail.aziz.currencyconverter.presentation.models.RateModel
import sohail.aziz.currencyconverter.presentation.models.RateViewModel
import sohail.aziz.currencyconverter.presentation.ui.view.base.AbstractViewHolder
import javax.inject.Inject

class RatesAdapter
@Inject constructor(
    private val context: Context,
    private val typeFactory: RatesViewTypeFactory
) : RecyclerView.Adapter<AbstractViewHolder<IRateVisitable>>() {

    private var ratesList: MutableList<RateViewModel> = mutableListOf()


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AbstractViewHolder<IRateVisitable> {
        val view = LayoutInflater.from(context).inflate(viewType, parent, false)
        return typeFactory.createViewHolder(view, viewType) as AbstractViewHolder<IRateVisitable>
    }

    override fun getItemId(position: Int): Long {
        return ratesList[position].id(typeFactory)
    }

    override fun getItemViewType(position: Int): Int {
        return ratesList[position].type(typeFactory)
    }

    override fun getItemCount(): Int {
        return ratesList.size
    }

    override fun onBindViewHolder(holder: AbstractViewHolder<IRateVisitable>, position: Int) {
        holder.bind(ratesList[position])
    }


    fun setData(ratesModels: List<RateModel>) {
        ratesList.clear()
        ratesList.addAll(
            ratesModels.map {
                RateViewModel(it, false)
            }
        )
        notifyDataSetChanged()
    }


}


