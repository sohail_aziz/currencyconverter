package sohail.aziz.currencyconverter.presentation.internal.di.component

import dagger.Subcomponent
import sohail.aziz.currencyconverter.presentation.internal.PerActivity
import sohail.aziz.currencyconverter.presentation.internal.di.module.MainActivityModule
import sohail.aziz.currencyconverter.presentation.ui.view.MainActivity


@PerActivity
@Subcomponent(modules = [MainActivityModule::class])
interface MainActivityComponent {


    //MainActivityComponent builder
    @Subcomponent.Builder
    interface Builder {
        fun mainActivityModule(module: MainActivityModule): MainActivityComponent.Builder

        fun build(): MainActivityComponent
    }

    fun inject(activity: MainActivity)


}