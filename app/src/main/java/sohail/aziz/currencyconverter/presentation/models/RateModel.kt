package sohail.aziz.currencyconverter.presentation.models

data class RateModel(val name: String, var value: Double)