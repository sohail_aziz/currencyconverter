package sohail.aziz.currencyconverter.presentation.ui.view

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.material.snackbar.Snackbar
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import sohail.aziz.currencyconverter.CurrencyConverterApplication
import sohail.aziz.currencyconverter.R
import sohail.aziz.currencyconverter.presentation.internal.di.component.MainActivityComponent
import sohail.aziz.currencyconverter.presentation.internal.di.module.MainActivityModule
import sohail.aziz.currencyconverter.presentation.models.RateModel
import sohail.aziz.currencyconverter.presentation.ui.presenter.IMainPresenter
import sohail.aziz.currencyconverter.presentation.ui.view.adapter.RatesAdapter
import sohail.aziz.currencyconverter.presentation.ui.view.base.ErrorMessageFactory
import timber.log.Timber
import javax.inject.Inject

class MainActivity : RxAppCompatActivity(), IMainView {

    @BindView(R.id.progressbar)
    lateinit var progress: ProgressBar
    @BindView(R.id.recyclerview_rates)
    lateinit var recyclerView: RecyclerView

    @Inject
    lateinit var presenter: IMainPresenter

    @Inject
    lateinit var adapter: RatesAdapter

    private var snackBar: Snackbar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        getInjector().inject(this)
        initView()


    }

    private fun initView() {

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    private fun getInjector(): MainActivityComponent {
        return CurrencyConverterApplication.getComponent(context = applicationContext)
            .mainActivityComponentBuilder()
            .mainActivityModule(module = MainActivityModule(activity = this))
            .build();
    }

    override fun showLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.GONE
    }

    override fun onRatesLoadSuccess(rates: List<RateModel>) {
        Timber.d("onRatesLoadSuccess")

        snackBar?.dismiss()
        adapter.setData(rates)
    }

    override fun onRatesLoadError(error: Throwable) {
        Timber.d("onRatesLoadError")

        error.printStackTrace()

        snackBar = Snackbar.make(
            findViewById(android.R.id.content),
            ErrorMessageFactory.getErrorMessage(error),
            Snackbar.LENGTH_INDEFINITE
        )

        snackBar?.show()

    }

    override fun scrollToPosition(position: Int) {
        recyclerView.layoutManager?.scrollToPosition(position)
    }
}
