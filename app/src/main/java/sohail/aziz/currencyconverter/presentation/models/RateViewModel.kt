package sohail.aziz.currencyconverter.presentation.models

import sohail.aziz.currencyconverter.presentation.ui.view.adapter.IRateViewTypeFactory
import sohail.aziz.currencyconverter.presentation.ui.view.adapter.IRateVisitable

class RateViewModel
constructor(var rateModel: RateModel, var isFocused:Boolean = false) : IRateVisitable {
    override fun type(factory: IRateViewTypeFactory): Int {
        return factory.type(this)
    }

    override fun id(factory: IRateViewTypeFactory): Long {
        return factory.id(this)
    }
}