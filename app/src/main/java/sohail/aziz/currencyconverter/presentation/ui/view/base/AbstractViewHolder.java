package sohail.aziz.currencyconverter.presentation.ui.view.base;

import android.content.Context;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.ButterKnife;

public abstract class AbstractViewHolder<T> extends RecyclerView.ViewHolder {

    @NonNull
    private final Context context;
    @Nullable
    private T boundElement;

    public AbstractViewHolder(@NonNull View parent) {
        super(parent);
        this.context = parent.getContext();
        ButterKnife.bind(this, parent);
    }

    @Nullable
    protected T getBoundValue() {
        return boundElement;
    }

    protected boolean hasBoundValue() {
        return boundElement != null;
    }

    public void bind(@NonNull T elementToBind) {
        this.boundElement = elementToBind;
    }

    public Context getContext() {
        return context;
    }

    public void onAttached() {
    }

    public void onDetached() {
    }
}
