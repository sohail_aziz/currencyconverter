package sohail.aziz.currencyconverter.presentation.ui.view

import sohail.aziz.currencyconverter.presentation.models.RateModel
import sohail.aziz.currencyconverter.presentation.models.RateViewModel

interface IMainView {

    fun showLoading();
    fun hideLoading();
    fun onRatesLoadSuccess(rates: List<RateModel>)
    fun onRatesLoadError(error: Throwable)
    fun scrollToPosition(position: Int)

}