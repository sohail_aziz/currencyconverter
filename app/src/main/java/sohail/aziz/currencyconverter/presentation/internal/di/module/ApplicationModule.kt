package sohail.aziz.currencyconverter.presentation.internal.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import sohail.aziz.currencyconverter.CurrencyConverterApplication
import sohail.aziz.currencyconverter.data.executor.JobExecutor
import sohail.aziz.currencyconverter.domain.executor.PostExecutionThread
import sohail.aziz.currencyconverter.domain.executor.ThreadExecutor
import sohail.aziz.currencyconverter.presentation.base.UIThread
import sohail.aziz.currencyconverter.presentation.internal.di.component.MainActivityComponent
import javax.inject.Singleton


@Module(subcomponents = [MainActivityComponent::class])
class ApplicationModule {

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun provideApplicationContext(application: CurrencyConverterApplication): Context = application.applicationContext

}