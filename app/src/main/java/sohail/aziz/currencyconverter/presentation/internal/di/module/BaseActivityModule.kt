package sohail.aziz.currencyconverter.presentation.internal.di.module

import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import sohail.aziz.currencyconverter.presentation.internal.PerActivity

@PerActivity
@Module
open class BaseActivityModule(private val activity: RxAppCompatActivity) {

    @PerActivity
    @Provides
    fun provideActivityLifecycle(): Observable<ActivityEvent> {
        return activity.lifecycle()
    }
}