package sohail.aziz.currencyconverter.presentation.ui.view.base

import sohail.aziz.currencyconverter.R
import java.io.IOException

object ErrorMessageFactory {

    fun getErrorMessage(throwable: Throwable): Int =
        when (throwable) {
            is IOException -> R.string.error_network
            else -> R.string.error_unknown
        }
}