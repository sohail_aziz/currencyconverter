package sohail.aziz.currencyconverter.presentation.internal.di.module

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import sohail.aziz.currencyconverter.data.repository.datastore.remote.CurrencyService
import sohail.aziz.currencyconverter.data.repository.CurrencyRepository
import sohail.aziz.currencyconverter.data.repository.datastore.remote.CurrencyApiDatastore
import sohail.aziz.currencyconverter.data.repository.datastore.remote.ICurrencyApiDatastore
import sohail.aziz.currencyconverter.domain.ICurrencyRepository
import javax.inject.Singleton


@Module
 class CurrencyModule {

    @Singleton
    @Provides
    fun provideCurrencyService(retrofit: Retrofit): CurrencyService {
        return retrofit.create(CurrencyService::class.java)
    }

    @Singleton
    @Provides
    fun provideCurrencyApiDatastore(currencyApiDatastore: CurrencyApiDatastore): ICurrencyApiDatastore =
        currencyApiDatastore

    @Singleton
    @Provides
    fun provideCurrencyRepository(currencyRepository: CurrencyRepository): ICurrencyRepository = currencyRepository

}