package sohail.aziz.currencyconverter.presentation.ui.view.base

import android.content.Context
import sohail.aziz.currencyconverter.R
import javax.inject.Inject

class CurrencyImageFactory
@Inject constructor(private val context: Context) {

    private val flagResMap: MutableMap<String, Int> = mutableMapOf()

    fun getFlagForCurrencyRes(name: String): Int {

        var resId = 0
        if (flagResMap.containsKey(name)) {
            resId = flagResMap[name]!!
        } else {
            resId =
                context.resources.getIdentifier(
                    name.toLowerCase(),
                    "drawable",
                    "sohail.aziz.currencyconverter"
                )
            if (resId == 0) resId = getFlagPlaceholder()

            flagResMap.put(name, resId)
        }
        return resId
    }

    fun getFlagPlaceholder(): Int = R.drawable.icon_flag
}