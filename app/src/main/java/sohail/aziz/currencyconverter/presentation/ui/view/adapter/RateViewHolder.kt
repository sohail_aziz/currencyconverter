package sohail.aziz.currencyconverter.presentation.ui.view.adapter

import android.view.View
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import sohail.aziz.currencyconverter.R
import sohail.aziz.currencyconverter.presentation.models.RateViewModel
import sohail.aziz.currencyconverter.presentation.ui.view.base.AbstractViewHolder
import sohail.aziz.currencyconverter.presentation.ui.view.base.CurrencyImageFactory
import timber.log.Timber


class RateViewHolder(
    parent: View,
    private val currencyImageFactory: CurrencyImageFactory
) : AbstractViewHolder<RateViewModel>(parent) {

    @BindView(R.id.view_currency)
    lateinit var currency: TextView
    @BindView(R.id.view_currency_description)
    lateinit var currencyDescription: TextView
    @BindView(R.id.view_flag)
    lateinit var currencyFlag: CircleImageView
    @BindView(R.id.view_amount)
    lateinit var amount: EditText


    override fun bind(elementToBind: RateViewModel) {
        super.bind(elementToBind)
        Timber.d("bind")

        val rateModel = elementToBind.rateModel

        currency.text = rateModel.name
        currencyDescription.text = rateModel.name

        val resId = currencyImageFactory.getFlagForCurrencyRes(rateModel.name)
        Picasso.with(context)
            .load(resId)
            .placeholder(currencyImageFactory.getFlagPlaceholder())
            .into(currencyFlag)

        amount.setText(rateModel.value.toString())
        amount.isEnabled = elementToBind.isFocused


    }

    companion object {
        const val LAYOUT = R.layout.layout_item_rate;
    }

}