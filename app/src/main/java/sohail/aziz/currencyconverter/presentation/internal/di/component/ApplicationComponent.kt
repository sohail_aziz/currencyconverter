package sohail.aziz.currencyconverter.presentation.internal.di.component

import dagger.BindsInstance
import dagger.Component
import sohail.aziz.currencyconverter.CurrencyConverterApplication
import sohail.aziz.currencyconverter.presentation.internal.di.module.ApplicationModule
import sohail.aziz.currencyconverter.presentation.internal.di.module.CurrencyModule
import sohail.aziz.currencyconverter.presentation.internal.di.module.NetworkModule
import javax.inject.Singleton


@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class, CurrencyModule::class])
interface ApplicationComponent {

    //injection point
    fun inject(application: CurrencyConverterApplication)

    //subComponents
    fun mainActivityComponentBuilder(): MainActivityComponent.Builder

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun currencyConverterApplication(application: CurrencyConverterApplication): Builder

        fun build(): ApplicationComponent
    }


}