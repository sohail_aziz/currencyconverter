package sohail.aziz.currencyconverter.extension

import java.math.RoundingMode
import java.text.DecimalFormat


fun Double.roundOffDecimal(): Double {
    val df = DecimalFormat("#.##")
    return df.format(this).toDouble()
}

fun Double.toFormattedString(): String {
    return String.format("%.2f", this)
}