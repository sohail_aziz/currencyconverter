package sohail.aziz.currencyconverter.data.repository.datastore.remote

import io.reactivex.Single
import sohail.aziz.currencyconverter.data.response.Rates
import javax.inject.Inject


class CurrencyApiDatastore
@Inject constructor(private val currencyService: CurrencyService) :
    ICurrencyApiDatastore {
    override fun refreshRates(baseCurrency: String): Single<Rates> {
        return currencyService.getRates(baseCurrency)
            .map { it.rates }
    }

}