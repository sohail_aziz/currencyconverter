package sohail.aziz.currencyconverter.data.mapper

import sohail.aziz.currencyconverter.data.response.Rates
import sohail.aziz.currencyconverter.extension.roundOffDecimal
import sohail.aziz.currencyconverter.presentation.models.RateModel
import javax.inject.Inject

class RatesToRatesModelMapper
@Inject constructor() {

    fun map(rates: Rates): List<RateModel> {

        val rateModels = mutableListOf<RateModel>()
        rateModels.add(RateModel(name = "AUD", value = rates.AUD.roundOffDecimal()))
        rateModels.add(RateModel(name = "BGN", value = rates.BGN.roundOffDecimal()))
        rateModels.add(RateModel(name = "BRL", value = rates.BRL.roundOffDecimal()))
        rateModels.add(RateModel(name = "CAD", value = rates.CAD.roundOffDecimal()))
        rateModels.add(RateModel(name = "CHF", value = rates.CHF.roundOffDecimal()))
        rateModels.add(RateModel(name = "CNY", value = rates.CNY.roundOffDecimal()))
        rateModels.add(RateModel(name = "CZK", value = rates.CZK.roundOffDecimal()))
        rateModels.add(RateModel(name = "DKK", value = rates.DKK.roundOffDecimal()))
        rateModels.add(RateModel(name = "GBP", value = rates.GBP.roundOffDecimal()))
        rateModels.add(RateModel(name = "HKD", value = rates.HKD.roundOffDecimal()))
        rateModels.add(RateModel(name = "HRK", value = rates.HRK.roundOffDecimal()))
        rateModels.add(RateModel(name = "HUF", value = rates.HUF.roundOffDecimal()))
        rateModels.add(RateModel(name = "IDR", value = rates.IDR.roundOffDecimal()))
        rateModels.add(RateModel(name = "ILS", value = rates.ILS.roundOffDecimal()))
        rateModels.add(RateModel(name = "INR", value = rates.INR.roundOffDecimal()))
        rateModels.add(RateModel(name = "ISK", value = rates.ISK.roundOffDecimal()))
        rateModels.add(RateModel(name = "JPY", value = rates.JPY.roundOffDecimal()))
        rateModels.add(RateModel(name = "KRW", value = rates.KRW.roundOffDecimal()))
        rateModels.add(RateModel(name = "MXN", value = rates.MXN.roundOffDecimal()))
        rateModels.add(RateModel(name = "MYR", value = rates.MYR.roundOffDecimal()))
        rateModels.add(RateModel(name = "NOK", value = rates.NOK.roundOffDecimal()))
        rateModels.add(RateModel(name = "NZD", value = rates.NZD.roundOffDecimal()))
        rateModels.add(RateModel(name = "PHP", value = rates.PHP.roundOffDecimal()))
        rateModels.add(RateModel(name = "PLN", value = rates.PLN.roundOffDecimal()))
        rateModels.add(RateModel(name = "RON", value = rates.RON.roundOffDecimal()))
        rateModels.add(RateModel(name = "RUB", value = rates.RUB.roundOffDecimal()))
        rateModels.add(RateModel(name = "SEK", value = rates.SEK.roundOffDecimal()))
        rateModels.add(RateModel(name = "SGD", value = rates.SGD.roundOffDecimal()))
        rateModels.add(RateModel(name = "THB", value = rates.THB.roundOffDecimal()))
        rateModels.add(RateModel(name = "TRY", value = rates.TRY.roundOffDecimal()))
        rateModels.add(RateModel(name = "USD", value = rates.USD.roundOffDecimal()))
        rateModels.add(RateModel(name = "ZAR", value = rates.ZAR.roundOffDecimal()))


        return rateModels
    }


}

