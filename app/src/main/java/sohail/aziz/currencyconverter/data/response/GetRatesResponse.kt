package sohail.aziz.currencyconverter.data.response

import com.squareup.moshi.Json


data class GetRatesResponse(
    @Json(name = "base") val base: String,
    @Json(name = "date") val date: String,
    @Json(name = "rates") val rates: Rates
)