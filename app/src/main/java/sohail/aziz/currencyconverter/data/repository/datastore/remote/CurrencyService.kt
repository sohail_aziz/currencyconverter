package sohail.aziz.currencyconverter.data.repository.datastore.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import sohail.aziz.currencyconverter.data.response.GetRatesResponse

interface CurrencyService {

    @GET("/latest")
    fun getRates(@Query("base") baseCurrency: String): Single<GetRatesResponse>
}