package sohail.aziz.currencyconverter.data.repository.datastore.remote

import io.reactivex.Single
import sohail.aziz.currencyconverter.data.response.Rates

interface ICurrencyApiDatastore {
    fun refreshRates(baseCurrency:String): Single<Rates>
}