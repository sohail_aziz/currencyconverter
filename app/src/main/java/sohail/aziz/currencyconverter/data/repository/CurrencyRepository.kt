package sohail.aziz.currencyconverter.data.repository

import io.reactivex.Single
import sohail.aziz.currencyconverter.data.mapper.RatesToRatesModelMapper
import sohail.aziz.currencyconverter.data.repository.datastore.remote.ICurrencyApiDatastore
import sohail.aziz.currencyconverter.domain.ICurrencyRepository
import sohail.aziz.currencyconverter.presentation.models.RateModel
import timber.log.Timber
import javax.inject.Inject

class CurrencyRepository
@Inject constructor(
    private val apiDatastore: ICurrencyApiDatastore,
    private val ratesToRatesModelMapper: RatesToRatesModelMapper
) : ICurrencyRepository {
    override fun refreshRates(baseCurrency: String): Single<List<RateModel>> {
        Timber.d("refreshRates: currency=" + baseCurrency)

        return apiDatastore.refreshRates(baseCurrency)
            .map { t -> ratesToRatesModelMapper.map(t) }

    }
}