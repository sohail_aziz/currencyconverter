package sohail.aziz.currencyconverter.domain.interactor

import com.trello.rxlifecycle2.android.ActivityEvent
import io.reactivex.Observable
import sohail.aziz.currencyconverter.Settings
import sohail.aziz.currencyconverter.domain.ICurrencyRepository
import sohail.aziz.currencyconverter.domain.executor.PostExecutionThread
import sohail.aziz.currencyconverter.domain.executor.ThreadExecutor
import sohail.aziz.currencyconverter.presentation.models.RateModel
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GetRatesUseCase
@Inject constructor(
    private val repository: ICurrencyRepository,
    threadExecutor: ThreadExecutor?,
    postExecutionThread: PostExecutionThread?,
    activityEventObservable: Observable<ActivityEvent>?
) : ActivityLifecycleUseCase<List<RateModel>>(
    threadExecutor,
    postExecutionThread,
    activityEventObservable
) {

    private lateinit var baseCurrency: String

    fun setBaseCurrency(base: String): GetRatesUseCase {
        baseCurrency = base
        return this
    }

    override fun buildUseCaseObservable(): Observable<List<RateModel>> {
        return Observable.interval(0, Settings.REFRESH_INTERVAL_IN_SECONDS, TimeUnit.SECONDS)
            .flatMap {
                repository.refreshRates(baseCurrency).toObservable()
                    //assumption -> it's netowrk error, all errors should be handled differently
                    .onErrorResumeNext { t: Throwable -> Observable.just(emptyList()) }

            }
    }
}


