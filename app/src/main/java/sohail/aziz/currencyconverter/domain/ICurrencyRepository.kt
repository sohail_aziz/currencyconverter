package sohail.aziz.currencyconverter.domain

import io.reactivex.Single
import sohail.aziz.currencyconverter.presentation.models.RateModel

interface ICurrencyRepository {

    fun refreshRates(baseCurrency:String): Single<List<RateModel>>

}