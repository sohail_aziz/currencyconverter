package sohail.aziz.currencyconverter

import android.app.Application
import android.content.Context
import sohail.aziz.currencyconverter.presentation.internal.di.component.ApplicationComponent
import sohail.aziz.currencyconverter.presentation.internal.di.component.DaggerApplicationComponent
import timber.log.Timber

class CurrencyConverterApplication() : Application() {

    lateinit var applicationComponent: ApplicationComponent
    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent
            .builder()
            .currencyConverterApplication(this)
            .build()

        Timber.plant(Timber.DebugTree())
    }

    fun getComponent(context: Context): ApplicationComponent {
        return applicationComponent;
    }

    companion object {
        fun getComponent(context: Context): ApplicationComponent {
            return (context as CurrencyConverterApplication).applicationComponent;
        }

        fun getApplication(context: Context): CurrencyConverterApplication {
            return (context as CurrencyConverterApplication)
        }
    }
}