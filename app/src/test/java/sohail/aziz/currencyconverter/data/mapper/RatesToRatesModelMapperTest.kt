package sohail.aziz.currencyconverter.data.mapper

import com.squareup.moshi.Moshi
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is
import org.junit.Before
import org.junit.Test
import sohail.aziz.currencyconverter.data.response.GetRatesResponse
import sohail.aziz.currencyconverter.presentation.models.RateModel

class RatesToRatesModelMapperTest {

    val mapper = RatesToRatesModelMapper()
    lateinit var rateList: List<RateModel>
    @Before
    fun setup() {
        val rateModels = getRatesResponse()

        rateList = mapper.map(rateModels.rates)
    }

    @Test
    fun testFirstItemMapped() {

        assertThat(rateList[0].name, Is.`is`("AUD"))//AUD":1.6154
        assertThat(rateList[0].value, Is.`is`(1.62)) //roundToDecimal


    }

    @Test
    fun testLastItemMapped() {

        assertThat(rateList[rateList.size - 1].name, Is.`is`("ZAR"))  //ZAR":17.812
        assertThat(rateList[rateList.size - 1].value, Is.`is`(17.81)) //roundToDecimal

    }

    @Test
    fun testItemSize() {

        assertThat(rateList.size, Is.`is`(32))
    }


    fun getRatesResponse(): GetRatesResponse {

        val response =
            "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6154,\"BGN\":1.9546,\"BRL\":4.7888,\"CAD\":1.5328,\"CHF\":1.1268,\"CNY\":7.9401,\"CZK\":25.699,\"DKK\":7.452,\"GBP\":0.89768,\"HKD\":9.1267,\"HRK\":7.4295,\"HUF\":326.29,\"IDR\":17313.0,\"ILS\":4.168,\"INR\":83.665,\"ISK\":127.72,\"JPY\":129.47,\"KRW\":1303.9,\"MXN\":22.351,\"MYR\":4.809,\"NOK\":9.7699,\"NZD\":1.7622,\"PHP\":62.553,\"PLN\":4.3156,\"RON\":4.6356,\"RUB\":79.525,\"SEK\":10.584,\"SGD\":1.599,\"THB\":38.106,\"TRY\":7.6234,\"USD\":1.1627,\"ZAR\":17.812}}"


        val moshi = Moshi.Builder().build()
        val adapter = moshi.adapter(GetRatesResponse::class.java)
        return adapter.fromJson(response)!!

    }
}